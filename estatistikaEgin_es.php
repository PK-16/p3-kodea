<?php
	require_once ('src/jpgraph.php');
	require_once ('src/jpgraph_pie.php');

	$konexioa = new mysqli("localhost","root","","pk");
	if($konexioa->connect_errno){
		die( "La conexión a MySQL ha fallado: (".
		$konexioa->connect_errno() . ") " .
		$konexioa->connect_error()	);
	}

	$irakasleakLortuSQL ="SELECT * FROM irakaslea";
	$ikasleakLortuSQL = "SELECT * FROM ikaslea";
	$informatikariakLortuSQL = "SELECT * FROM informatikaria";
	$familiakLortuSQL = "SELECT * FROM familia";

	$irakasleakLortu = $konexioa -> query ($irakasleakLortuSQL);
	$ikasleakLortu = $konexioa -> query ($ikasleakLortuSQL);
	$informatikariakLortu = $konexioa -> query ($informatikariakLortuSQL);
	$familiakLortu = $konexioa -> query ($familiakLortuSQL);

	$irakasleakKop = mysqli_num_rows($irakasleakLortu);
	$ikasleakKop = mysqli_num_rows($ikasleakLortu);
	$informatikariakKop = mysqli_num_rows($informatikariakLortu);
	$familiakKop = mysqli_num_rows($familiakLortu);

	$datuak = array($irakasleakKop, $ikasleakKop, $informatikariakKop, $familiakKop);
	//$datuak = array(75, 15);

	$grafikoa = new PieGraph(350,350);

	$theme = "Default Theme";

	$grafikoa -> title-> Set ("Estadísticas");
	$grafikoa -> SetBox(true);

	$p1 = new PiePlot($datuak);
	$grafikoa -> add($p1);

	$p1 -> ShowBorder();
	$p1 -> SetColor ('black');
	$p1 -> SetSliceColors(array('#FF0000','#2E8B57','#2E64FE', '#FFFF00'));
	$p1 -> SetLegends(array("Profesores","Alumnos", "Informáticos","Familias"));
	$p1 -> value -> SetFont(FF_FONT0);
	$grafikoa -> legend -> SetPos(0.6,0.98,'right','bottom');
	$grafikoa->Stroke();
?>
