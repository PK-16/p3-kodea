-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-03-2016 a las 15:49:59
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pk`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bideoa`
--

CREATE TABLE `bideoa` (
  `id` int(11) NOT NULL,
  `link` varchar(1000) NOT NULL,
  `mota` varchar(100) NOT NULL,
  `likeKop` int(11) NOT NULL,
  `dislikeKop` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `bideoa`
--

INSERT INTO `bideoa` (`id`, `link`, `mota`, `likeKop`, `dislikeKop`) VALUES
(1, 'https://player.vimeo.com/video/155250438', 'pasahitza segurua', 4, 2),
(2, 'https://www.youtube.com/embed/wMIRC0RY66U', 'pasahitza segurua', 0, 0),
(3, 'https://www.youtube.com/embed/0PGzmapJrls', 'sare sozialak', 0, 0),
(4, 'https://player.vimeo.com/video/155396352', 'pasahitza segurua / konexio segurua', 2, 1),
(5, 'https://drive.google.com/file/d/0B8mjq34CpRgTYWlnMm80TmU1Mms/preview', 'sare sozialak / pribatutasuna', 0, 0),
(6, 'https://www.youtube.com/embed/QgTS-FWibsg', 'sare sozialak / pribatutasuna', 0, 0),
(7, 'https://drive.google.com/file/d/0B-EGua8BQAAuQV9JbXVlc3dDQ1E/preview', 'pasahitza segurua / pribatutasuna', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familia`
--

CREATE TABLE `familia` (
  `herrialdea` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `familia`
--

INSERT INTO `familia` (`herrialdea`) VALUES
('China');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ikaslea`
--

CREATE TABLE `ikaslea` (
  `herrialdea` varchar(100) NOT NULL,
  `maila` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ikaslea`
--

INSERT INTO `ikaslea` (`herrialdea`, `maila`) VALUES
('Espainia', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informatikaria`
--

CREATE TABLE `informatikaria` (
  `herrialdea` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `informatikaria`
--

INSERT INTO `informatikaria` (`herrialdea`) VALUES
('Espainia'),
('Frantzia'),
('Canada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `irakaslea`
--

CREATE TABLE `irakaslea` (
  `herrialdea` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `irakaslea`
--

INSERT INTO `irakaslea` (`herrialdea`) VALUES
('Frantzia'),
('EEUU');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bideoa`
--
ALTER TABLE `bideoa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bideoa`
--
ALTER TABLE `bideoa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
