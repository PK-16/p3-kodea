<?php 
	//Konexioa egiteko
	$konexioa = new mysqli("localhost","root","","pk");
	if($konexioa->connect_errno){
		die( "Huts egin du konexioak MySQL-ra: (". 
		$konexioa->connect_errno() . ") " . 
		$konexioa->connect_error()	);
	}
	
	//Lortu bideo motak
	$motak = $_GET['mota'];
	$luzeeraDatua = strlen($motak);
	
	//SQL aginduak
	$bideoGuztiak = "Select * from bideoa";
	
	if($motak == ""){ //Bideo guztiak pantailaratu
		$balioak = $konexioa -> query($bideoGuztiak);
		while($linka = mysqli_fetch_assoc($balioak)){
			echo "
				<div>
					<iframe width='400' height='200' src=".$linka['link']." frameborder='0' allowfullscreen></iframe>
					<br/>
					<input type='image' src='irudiak/like.png' style='margin-left:2cm' onclick='likeGehitu(id)' id='".$linka['id']."gehitu'/>
					<label style='padding:1cm;' id='".$linka['id']."likeKop'>".$linka['likeKop']."</label>
					<input type='image' src='irudiak/dislike.png' style='margin-left:1cm' onclick='dislikeGehitu(id)'id='".$linka['id']."kendu'/>
					<label style='padding:1cm' id='".$linka['id']."dislikeKop'>".$linka['dislikeKop']."</font>
				</div>
			";
		}
	}else{
		
		$bilatzekoMotak = explode(" / ", $motak); //Mota bat baino gehiago. Banatu /
		$bilatzekoKop = count($bilatzekoMotak); //Genero kopurua kontatu
		
		$balioak = $konexioa -> query($bideoGuztiak);  //Bideo guztiak lortu
		
		while($linka = mysqli_fetch_assoc($balioak)){ //Korritu bideo bakoitza
			$linkenGeneroa = explode(" / ", $linka['mota']); //Banatu bideoen motak /
			$linkenGeneroKop = count($linkenGeneroa); //Kontatu zenbat diren generoak
			
			if($bilatzekoKop <= $linkenGeneroKop){ 
				$motaBerdinak=1;
				for ($i=0; $i < $bilatzekoKop; $i++) { 
					$momentukoMotaBilatuta = 0;
					for ($j=0; $j < $linkenGeneroKop; $j++) { 

						if(strcmp($bilatzekoMotak[$i],$linkenGeneroa[$j])==0){
							$momentukoMotaBilatuta = 1;
							break;
						}
					}
					if($momentukoMotaBilatuta == 0){
						$motaBerdinak=0;
						break;
					}
				}
				if($motaBerdinak==1){
					echo "
					<div>
						<iframe width='400' height='200' src=".$linka['link']." frameborder='0' allowfullscreen></iframe>
						<br/>
						<input type='image' src='irudiak/like.png' style='margin-left:2cm' onclick='likeGehitu(id)' id='".$linka['id']."gehitu'/>
						<label style='padding:1cm;' id='".$linka['id']."likeKop'>".$linka['likeKop']."</label>
						<input type='image' src='irudiak/dislike.png' style='margin-left:1cm' onclick='dislikeGehitu(id)'id='".$linka['id']."kendu'/>
						<label style='padding:1cm' id='".$linka['id']."dislikeKop'>".$linka['dislikeKop']."</font>
					</div>
					";
				}
			}
		}
		
	}
?>